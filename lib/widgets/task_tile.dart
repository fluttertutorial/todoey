import 'package:flutter/material.dart';


class TaskTile extends StatelessWidget {

  bool isChecked = false;
  final String taskTitle;
  final Function checkBoxCallback;

  TaskTile({this.taskTitle, this.isChecked, this.checkBoxCallback});


  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(
        'This is a title',
        style: TextStyle(
          decoration: isChecked ? TextDecoration.lineThrough : null
        ),
      ),
      trailing: Checkbox(
        activeColor: Colors.lightBlueAccent,
        value: isChecked,
        onChanged: checkBoxCallback,
      )
    );
  }
}

//
//class TaskCheckBoxState extends StatelessWidget{
//
//  final bool checkBoxState;
//  final Function toggleCheckBoxState;
//
//  TaskCheckBoxState(this.checkBoxState, this.toggleCheckBoxState);
//
//  @override
//  Widget build(BuildContext context) {
//    return Checkbox(
//      activeColor: Colors.lightBlueAccent,
//      value: checkBoxState,
//      onChanged: toggleCheckBoxState,
//    );
//  }
//}
